# My Ultimate vimrc
***
## Prerequisites
> You should have installed git in order to clone plugins

## Installation
First clone vundle, in order to manage all plugins
```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

Then you can put the vimrc file into your home directory or use a symlink
```bash
ln -s /path/to/clone/vimrc ~/.vimrc
```

Then open vim and run:
```bash
:PluginInstall
```
Now restart vim and enjoy!

> To update the plugins, run `:PlugingUpdate` in vim

