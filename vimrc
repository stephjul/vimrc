"  .vimrc
"
"  Copyright (C) 2017 Julien Stephan
"
"
" This program is free software: you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation, either version 3 of the License, or
" (at your option) any later version.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program.  If not, see <http://www.gnu.org/licenses/>



" Use vundle to manage plugin
set nocompatible              " required for vundle to work properly
filetype off                  " required for vundle to work properly

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" plugin on GitHub repo

" Git  vim-fugitive
Plugin 'tpope/vim-fugitive'

" Auto align
Plugin 'godlygeek/tabular.git'

"NerdTree
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'

" Indent guides
" Plugin 'nathanaelkane/vim-indent-guides'
"ctrlp
Plugin 'ctrlpvim/ctrlp.vim'

"Surround vim
Plugin 'tpope/vim-surround'

"File-line
Plugin 'bogado/file-line'

"SnipMate
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'

" Optional a lot of predefined snippets
Plugin 'honza/vim-snippets'

" A must have
Plugin 'editorconfig/editorconfig-vim'

" vim look
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Damned pretty font for nerdtree and vim-airline
" => use https://github.com/ryanoasis/nerd-fonts for fonts
Plugin 'ryanoasis/vim-devicons'

"Smart Tabs
Plugin 'vim-scripts/Smart-Tabs'


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
"
" see :h vundle for more details or wiki for FAQ

syntax on

" Remove StripTrailingWhitespaces
fun! <SID>StripTrailingWhitespaces()
	let l = line(".")
	let c = col(".")
	%s/\s\+$//e
	call cursor(l, c)
endfun
autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()

" NERDTree plugin configuration
map <silent> <F2> :NERDTreeToggle<CR>
" Quit if NERDTree is the only window left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" vim-airline configuration
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
set t_Co=256
let g:bufferline_echo = 0
if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"

"TODO verify Indentguide otherwise use:
set listchars=tab:\|\  "comment needed here as I strip Trailing Whitespaces
set nolist "don't display indentGuides, can be displayed using <F3>

let s:indentGuidesEnabled = 0
function! ToggleIndentGuides()
	if s:indentGuidesEnabled
		set nolist
		let s:indentGuidesEnabled = 0
	else
		set list
		let s:indentGuidesEnabled = 1
	endif
endfunction
noremap <silent> <F3> :call ToggleIndentGuides()<CR>

" Indentation guide colors
"let g:indent_guides_auto_colors=0
"hi IndentGuidesOdd  ctermbg=235
"hi IndentGuidesOdd  ctermbg=red
"hi IndentGuidesEven ctermbg=233
"hi IndentGuidesEven ctermbg=blue
" Indentation guides
"let g:indent_guides_guide_size=1
"let g:indent_guides_start_level=2
"noremap <silent> <A-g> :IndentGuidesToggle<CR>

" Map for moving easily between split window
noremap <C-left> <C-w><left>
noremap <C-right> <C-w><right>
noremap <C-up> <C-w><up>
noremap <C-down> <C-w><down>

" hightligh current line
set cursorline

"Highlight columns above 80
highlight ColorColumn ctermbg=236 guibg=#2c2d27
let s:rightMarginEnabled = 0
function! ToggleRightMargin()
	if s:rightMarginEnabled
		let &colorcolumn=0
		let s:rightMarginEnabled = 0
	else
		let &colorcolumn=join(range(81,999),",")
		let s:rightMarginEnabled = 1
	endif
endfunction
noremap <silent> <F4> :call ToggleRightMargin()<CR>

" allow to switch buffer even if not saved
set hidden

" numerotation
set number
set relativenumber

" for highlight
set hls
" Press Space to turn off highlighting and clear any message already
" displayed.
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>
" case insensitive search
set ignorecase
set smartcase
set incsearch

" indentation see smart tab plugin to indent with tab and align with space
:set shiftwidth=4
:set tabstop=4
:set softtabstop=4

" Mouse support
set mouse=a

" For vimdiff (and vim signify)
highlight DiffAdd cterm=none ctermfg=green ctermbg=none
highlight DiffDelete cterm=none ctermfg=red ctermbg=none
highlight DiffChange cterm=none ctermfg=blue ctermbg=none
highlight SignColumn none

" for lua file
autocmd FileType lua setlocal  shiftwidth=2 tabstop=2 softtabstop=2

" snipMate configuration
imap ² <Plug>snipMateNextOrTrigger
smap ² <Plug>snipMateNextOrTrigger
